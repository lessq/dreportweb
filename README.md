When deploying, create a user for run dReport HTTP server, and install all programs under the users home directory.

## Install
This project requires node.js v6.9.1.
If it was already installed, just hit `npm install` and go to 'Run server' section.

### Install nvm
Run `scripts/install_nvm.sh`.
The scrpit clones nvm.git from github into ~/.nvm, and appends a command to `~/.bashrc`.

If your are not using bash, you should fix the script.

* Line:3 Define config file path.
* Line:21-35 the command appended.

### Install packages
Install packages the project depends on including node.js.
Run `scripts/install_packages.sh`.


### Setup MySQL
Create a database, a database user, and tables.
Run `script/setup_mysql.sh`

The default administrators name/pass is test/test.

## Run server
The easiest way is hit `npm run watch`.
You can implement incrementaly with this command.

In default, the server listening port 3000.
Server configuration is written in `settings.json`.

This command do:

1. Builds project. (`npm run build`)
2. Run dReport HTTP server. (`npm run start`)
3. Watch files in src/, and restart server after rebuild when file changed.