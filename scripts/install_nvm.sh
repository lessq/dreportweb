#!/bin/sh

RCFILE=~/.bashrc

if [ "${NVM_DIR:-}" = '' ]; then
    # not loaded
    if [ -s ~/.nvm/nvm.sh ]; then
        # installed
        if nvm --help > /dev/null; then
            echo 'nvm is already installed.'
        else
            echo 'nvm is already installed, but not loaded.'
            echo 'Load ~/.nvm/nvm.sh on your shell.'
        fi
    else
        echo "'nvm' is not installed."
        git clone git://github.com/creationix/nvm.git ~/.nvm
        cat <<EOF >> $RCFILE
# Loading nvm.sh takes a second.
# This function makes 'nvm' command to lazy load.
nvm() {
    if [ "${NVM_DIR:-}" = '' ]; then
        if [ -s ~/.nvm/nvm.sh ]; then
            source ~/.nvm/nvm.sh
            nvm $@
        else
            echo "'nvm' is not installed."
            echo "To install nvm, do:"
            echo 'git clone git://github,com/creationix/nvm.git ~/.nvm'
        fi
    else
        source $NVM_DIR/nvm.sh
        nvm $@
    fi
}
EOF
    fi
else
    echo 'nvm is already installed.'
fi
