#!/bin/sh

ROOT=`dirname $0`/..
SETTING="$ROOT/settings.json"

DB_NAME="dReport"
DB_USER_NAME="dReport"
DB_PASSWORD=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1`
# DDL=`cat $ROOT/db/table_definitions.sql`
DDL=`cat $ROOT/db/dump.sql`

echo 'Create database and user...'
mysql -u root -p <<EOF
DROP DATABASE IF EXISTS ${DB_NAME};
DROP USER IF EXISTS '${DB_USER_NAME}'@localhost;

CREATE USER '${DB_USER_NAME}'@localhost IDENTIFIED BY '${DB_PASSWORD}';
CREATE DATABASE ${DB_NAME} CHARACTER SET utf8;
GRANT ALL ON ${DB_NAME}.* to '${DB_USER_NAME}'@localhost;

USE ${DB_NAME};

${DDL}
EOF
echo 'Create database and user... Done.'

js=`cat <<EOF
var setting;
try {
  setting = JSON.parse(fs.readFileSync('${SETTING}', 'utf8'));
} catch(e) {
  setting = {
    app: { port: 3000 },
    db: { host: 'localhost', port: 3306 }
  };
}

var db = setting.db;
db.database = '${DB_NAME}';
db.user     = '${DB_USER_NAME}';
db.password = '${DB_PASSWORD}';

fs.writeFileSync('${SETTING}', JSON.stringify(setting, null, '    '));
EOF
`


node -e "${js}"
# echo $js
