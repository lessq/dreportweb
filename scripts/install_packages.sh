#!/bin/sh

NODE_VERSION="v6.9.1"
PROJECT_ROOT=`dirname $0`/..

echo 'Load nvm.'
if [ "${NVM_DIR:-}" = '' ]; then
    if [ -s ~/.nvm/nvm.sh ]; then
        . ~/.nvm/nvm.sh
    else
        echo "'nvm' is not installed."
        echo "To install nvm, do:"
        echo 'git clone git://github,com/creationix/nvm.git ~/.nvm'
        exit 1
    fi
else
    nvm --help > /dev/null 2>&1 || \
        . $NVM_DIR/nvm.sh
fi
echo 'Load nvm. Done.'

echo 'Install node.'
nvm install $NODE_VERSION
echo 'Install node. Done.'

echo 'Install npm packages.'
cd $PROJECT_ROOT
npm install
echo 'Install npm packages. Done.'

echo 'Finish installing all package.'
