/// <reference path="./typings/index.d.ts" />;
import express = require('express');
import mime = require('mime');
import path = require('path');
import bodyParser = require('body-parser'); // to read request body
import cookieParser = require('cookie-parser');
import fs = require('fs');
import crypto = require('crypto');
// import http = require('http');
import mysql = require('mysql');
// import node_uuid = require('node-uuid');
// import iconv = require('iconv');
// import date_utils = require('date-utils');
// let DATE_UTILS = date_utils; // need for use date-utils;

import { Mutex } from './mutex';
import { Settings } from './settings';
import { Http } from './http';


//// Init application.
var setting = Settings.load('settings.json');

var app = express();
export var db = mysql.createPool(setting.db);
var mutex = new Mutex();
var dapi = new Http(setting.dapi.host + ':' + setting.dapi.port);

import { query, withTransaction, log, log_error, returnQueryResult, sha256, returnJSON, useQueryResults, wait, secureRandom } from './utils';

//// Set middlewars.
app.use(bodyParser.json(({ limit: '50mb' })));
app.use(cookieParser());

// Route static contents.
app.use('/assets', express.static('dest/assets'));
app.use(/^\/(inline|styles|vendor|main)(\.[a-z0-9]*)?\.bundle\.(map|js|css)$/, (function(){
  let statics = {};
  return function(req: express.Request, res, next) {
    let fn: express.Handler = statics[req.baseUrl];
    if (!fn) {
      fn = express.static('dest' + req.baseUrl);
      statics[req.baseUrl] = fn;
    }
    fn(req, res, next);
  };
})());
app.use('/style.css', express.static('dest/style.css'));
app.use('/node_modules/', express.static('node_modules/'));
app.use('/browser-sync', express.static('node_modules/browser-sync'));
app.use('/login', express.static('dest/index.html'));
app.use('/favicon.ico', express.static('dest/favicon.ico'));


/// Define API routes.
interface Request extends express.Request {
  isAdmin: boolean,
  loginUserId: number;
  loginUserName: string;
  sessionId: number;
  sessionKey: string;
  contentId: string;
}

app.param('contentId', (req: Request, res, next, val?) => {
  req.contentId = val;
  next();
});

app.route('*')
  .all((req: Request, res, next) => {
    let session_key = req.cookies.session;
    if (!session_key)
      return next();
    req.sessionKey = session_key;
    query(`
SELECT
  u.name,
  u.id,
  s.id sid,
  EXISTS (SELECT * FROM UserType WHERE user_id = u.id AND user_type_id = 1) isAdmin
FROM
  Session s
  JOIN User u ON s.user_id = u.id
WHERE
  s.session_key = ?
  AND s.expiry > NOW()
  AND NOT s.closed
`, session_key)
      .then(row => row[0])
      .then(ret => {
        if (ret) {
          req.loginUserId = ret.id;
          req.loginUserName = ret.name;
          req.sessionId = ret.sid;
          req.isAdmin = ret.isAdmin;
        }
        next();
      });
  })
  .all((req: Request, res, next) => {
    query(`
INSERT INTO Log (
  user_id,
  http_method,
  session_key,
  ip,
  user_name,
  path
) VALUES (?)
`, [
  req.loginUserId,
  req.method,
  req.sessionKey,
  req.ip,
  req.loginUserName,
  req.path
])
    next();
  });

app.route('/logout')
  .get((req: Request, res) => {
    if (!req.sessionId)
      return res.redirect('/login');
    query('UPDATE Session SET closed = true WHERE id = ?', req.sessionId)
      .then(() => {
        let cookie_options = {
          expires: new Date(Date.now() - (1000 * 60 * 60 * 24 * 30))
        };
        res.cookie('session', '', cookie_options);
        res.redirect('/login')
      })
      .catch(e => {
        log_error(e);
        res.status(500).end();
      });
  });

app.route('/api/login')
  .get((req: Request, res) => {
    if (req.loginUserId)
      return returnQueryResult(res,
                               'SELECT id, user_id, name, address FROM User WHERE id = ?',
                               req.loginUserId);
    else
      return returnJSON(res, null);
  })
  .post(async (req: Request, res, next) => {
    // Wait for 1 sec for birthday attack.
    try {
      await wait(1000);

      if (!req.body.userId ||
          !req.body.password)
        return res.status(404).end();

      let userInfo = await query(`SELECT * FROM User WHERE user_id = ? AND enabled = true`, req.body.userId)
        .then(r => r[0]);

      if (!userInfo)
        return res.status(404).end();

      let hash = sha256(req.body.password, userInfo.salt);

      if (hash !== userInfo.password)
        return res.status(404).end();

      let session_key = await secureRandom(32, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
      if (req.cookies.session)
        await query('UPDATE Session SET closed = true WHERE session_key = ?', req.cookies.session);


      let cookie_options = {
        // default, 30 days.
        expires: new Date(Date.now() + 1000 * 60 * 60 * 24 * 30)
      };
      res.cookie('session', session_key, cookie_options);

      await query('INSERT INTO Session (user_id, session_key, expiry) VALUES (?)',
                  [userInfo.id, session_key, cookie_options.expires]);
      res.status(200).end();
    } catch(e) {
      log_error(e);
      res.status(404).end();
    }
  });

// If not loged in, returns 404 only.
app.route('/api/*')
  .all((req: Request, res, next) => {
    if (req.loginUserId)
      next();
    else
      res.status(404).end();
  });

app.route('/api/admin/isAdmin')
  .get((req: Request, res) => {
    res.json(req.isAdmin);
  });

app.route('/api/admin/*')
  .all((req: Request, res, next) => {
    if (req.isAdmin)
      next();
    else
      res.status(404).end();
  });

app.route('/api/admin/users')
  .get(useQueryResults(`
SELECT
  u.id,
  u.user_id,
  u.name,
  u.address
FROM
  User u
  JOIN UserType t ON u.id = t.user_id
WHERE
  t.user_type_id IN (1, 2)
  AND enabled = true
`));
app.route('/api/admin/centers')
  .get(useQueryResults(`
SELECT
  u.id,
  u.user_id center_id,
  u.name,
  u.address
FROM
  User u
  JOIN UserType t ON u.id = t.user_id
WHERE
  t.user_type_id = 2
  AND enabled = true;
`));
app.route('/api/admin/centers/:contentId')
  .get((req: Request, res) => {
    returnQueryResult(res, `
SELECT
  u.id,
  u.user_id center_id,
  u.name,
  u.address
FROM
  User u
  JOIN UserType t ON u.id = t.user_id
WHERE
  u.id = ?
`, req.contentId);
  })
  .delete((req: Request, res) => {
    let uid = parseInt(req.contentId);
    if (uid === req.loginUserId)
      return res.json(false);

    withTransaction(con => {
      return query(con, 'UPDATE User SET enabled = false WHERE id = ?', uid);
    })
      .then(() => res.json(true))
      .catch(() => res.status(500).end());
  });

app.route('/api/admin/centers')
  .post((req, res) => {
    let json = <any[]>req.body;
    if (!(json instanceof Array))
      return res.status(400).end();

    withTransaction(con => {
      async function normalize(center):
      Promise<{
        id;
        user_id;
        name;
        address;
        salt?;
        password?
      }> {
        if (center.password) {
          let salt = await secureRandom();
          let pass = sha256(center.password, salt);
          return {
            id: center.id,
            user_id: center.center_id,
            name: center.name,
            address: center.address,
            salt: salt,
            password: pass
          };
        }
        else
          return {
            id: center.id,
            user_id: center.center_id,
            name: center.name,
            address: center.address,
          };
      }

      let newCenters = json.filter(c => !c.id)
        .map(normalize)
        .map(async center => {
          let c = await center;
          let res = await query(con, `
INSERT INTO User (
  user_id,
  name,
  address,
  salt,
  password,
  enabled
) VALUES (?)`, [c.user_id, c.name, c.address, c.salt, c.password, true]);
          return await query(con, `INSERT INTO UserType (user_id, user_type_id) VALUES (?, 2)`,
                             res.insertId);
        });
      let oldCenters = json.filter(c => c.id)
        .map(normalize)
        .map(center =>
             center.then(c => query(con, `UPDATE User SET ? WHERE id = ?`,
                                    c, c.id)));

      return Promise.all(newCenters.concat(oldCenters));
    })
      .then(() => res.status(200).end())
      .catch(e => {
        log_error(e);
        res.status(500).end();
      });
  });

app.route('/api/pdf/:contentId')
  .get((req: Request, res) => {
    // FIXME: This is a mock.
    let filename = req.query.uhid;
    let date = new Date(Date.now());
    let timestamp = [
      date.getUTCMonth() + 1,
      date.getUTCDate(),
      date.getUTCHours(),
      date.getUTCMinutes(),
      date.getUTCSeconds()
    ]
      .map(n => ('0' + n).slice(-2))
      .join('-');
    if (filename)
      filename += '-' + timestamp;
    else
      filename = timestamp;

    filename += '.pdf';

    res.setHeader('Content-Disposition', `attachment; filename*=UTF-8''${filename}`);
    res.sendFile(path.resolve('static/sample.pdf'));
  });
app.route('/api/pdf/:contentId/exists')
  .get((req: Request, res) => {
    // FIXME: This is a mock.
    if (req.contentId.length > 2 &&
        parseInt(req.contentId) %2 === 0)
      res.json(true);
    else
      res.json(false);
  });
app.route('/api/pdf/:contentId/image')
  .get((req: Request, res) => {
    // FIXME: This is a mock.
    dapi.getBuffer('/report/' + req.contentId)
      .then(buf => {
        res.write(buf);
        res.end();
      });
  });

app.get('/api/invoices/:contentId/exists', (req: Request, res) => {
  dapi.get('/invoices/' + req.contentId + '/exists')
    .then(JSON.parse)
    .then(json => {
      if (json.status == 0)
        res.json(json.value);
      else
        throw json;
    })
    .catch(e => {
      log_error(e);
      res.status(500).end()
    });
});
app.get('/api/invoices/:contentId', (req: Request, res) => {
  dapi.get('/invoices/' + req.contentId)
    .then(JSON.parse)
    .then(json => {
      if (json.status == 0)
        res.json(json.value);
      else
        throw json;
    })
    .catch(e => {
      log_error(e);
      res.status(500).end();
    });
});

// Fall back.
app.route('/api/*')
  .all((req, res) => {
    res.status(404).end();
  });

app.route('/')
  .get((req: Request, res) => {
    if (req.isAdmin)
      res.redirect('/admin');
    else
      res.redirect('/centers/' + req.loginUserId);
  });

// All page in Angular2 requires index.html.
app.get('*', function renderIndex(req: Request, res: express.Response, next) {
  res.sendFile(path.resolve('dest/index.html'));
});

//// Start server.
var server = app.listen(setting.app.port, () => {
  var host = server.address().address;
  var port = server.address().port;
  console.log('This express app is running on: ' + host);
  console.log('Listening on port:' + port);
});
