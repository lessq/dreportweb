import mysql = require('mysql');
import crypto = require('crypto');
import express = require('express');
import node_uuid = require('node-uuid');

import { db } from './main';

interface IPromiseResolve<T> {
  (value?: T | PromiseLike<T>): void;
}
interface IPromiseReject {
  (reason?: any): void;
}

export function promise<T>(cb: (resolve: IPromiseResolve<T>, reject: IPromiseReject) => void) {
  return new Promise<T>(cb)
    .catch(e => Promise.reject(log_error(e)));
}
export function does(cb, ...args) {
  return () => cb(...args);
}
export function log<T>(obj: T): T {
  console.log(obj);
  return obj;
}
export interface Exception extends Error {
  wasShown: boolean
}
export function log_error(e: Exception) {
  if (e instanceof Error) {
    if (!e.wasShown) {
      console.error(e.stack);
      e.wasShown = true;
      return e;
    }
  } else
    console.error(e);
  return e;
}

export function query(sql: string, ...args): Promise<any>;
export function query<T>(sql: string, ...args): Promise<T>;
export function query(con: mysql.IConnection, sql: string, ...args): Promise<any>;
export function query<T>(con: mysql.IConnection, sql: string, ...args): Promise<T>;
export function query<T>(a, b, ...c) {
  let con, sql, args;
  if (typeof a === 'string') {
    con = db;
    sql = a;
    if (c.length > 0)
      c.unshift(b);
    else
      c = [b];
    args = c;
  } else {
    con = a;
    sql = b;
    args = c;
  }
  return promise<T>((resolve, reject) => {
    con.query(sql, args, (error, results) => {
      if (error)
        reject(error);
      else
        resolve(results);
    });
  })
    .catch(e => {
      console.error(mysql.format(sql, args));
      return Promise.reject(e);
    });
}

export function queryOne(sql: string, ...args): Promise<any>;
export function queryOne<T>(sql: string, ...args): Promise<T>;
export function queryOne(con: mysql.IConnection, sql: string, ...args): Promise<any>;
export function queryOne<T>(con: mysql.IConnection, sql: string, ...args): Promise<T>;
export function queryOne<T>(a, b, ...c) {
  return query(a, b, ...c)
    .then(rows => rows[0])
}

export function beginTransaction(con: mysql.IConnection) {
  return promise<mysql.IConnection>((resolve, reject) => {
    con.beginTransaction(err => {
      if (err)
        reject(err);
      else
        resolve(con)
    });
  });
};

export function commit(con: mysql.IConnection) {
  return promise<mysql.IConnection>((resolve, reject) => {
    con.commit(err => {
      if (err)
        reject(err);
      else
        resolve(con);
    });
  });
}

export function withTransaction<T>(cb: (con: mysql.IConnection) => T | Promise<T>, err_cb?: ((error:any) => void)) {
  let connection: mysql.IConnection;
  return  promise<T>((resolve, reject) => {
    db.getConnection((err, con) => {
      connection = con;
      if (err)
        throw err;

      beginTransaction(con)
        .then(cb)
        .then(val => commit(con).then(() => resolve(val)))
        .then(() => con.release())
        .catch(reject);
    });
  })
    .catch(e => {
      if (connection)
        connection.rollback(() => connection.release());
      if (err_cb)
        return Promise.reject(err_cb(e));
      else
        return Promise.reject(log_error(e));
    });
}

export function uuid(): string {
  return node_uuid.v4();
}
export function secureRandom(length = 16, map = 'ACDEFGHJKLMNPQRSTUVWXY'): Promise<string> {
  return promise<string>((resolve, reject) => {
    crypto.randomBytes(length, (err, buf) => {
      if (err)
        return reject(err);
      let tmp = new Array(length);
      for (let i = 0; i < length; i++)
        tmp[i] = map[buf[i] % map.length];
      resolve(tmp.join(''));
    });
  });
}
export function sha256(target: string, salt: string) {
  let sha256 = crypto.createHash('sha256');
  sha256.update(target);
  sha256.update(salt);
  return sha256.digest('hex');
}

export function wait(milsec: number) {
  return promise<void>(r => setTimeout(r, milsec));
}

export function returnJSON<T>(res: express.Response, p: T | Promise<T>) {
  return Promise.resolve(p)
    .then(res.json.bind(res))
    .catch(() => res.status(500).end());
}

export function returnQueryResult(res: express.Response, sql: string, ...args) {
  return returnJSON(res, query(sql, ...args).then(rows => rows[0]));
}

export function returnQueryResults(res: express.Response, sql: string, ...args) {
  return returnJSON(res, query(sql, ...args));
}

export function useQueryResult(sql: string, ...args) {
  return (req: express.Request, res: express.Response) => {
    return returnQueryResult(res, sql, ...args);
  };
}

export function useQueryResults(sql: string, ...args) {
  return (req: express.Request, res: express.Response) => {
    return returnQueryResults(res, sql, ...args);
  };
}

export class Mutex {
  private queue: Promise<void>[] = [];

  lock(cb: (unlock: () => void) => void) {
    let resolve;
    let promise = (new Promise<Mutex>(r => {
      resolve = r;
    }))
      .then(() => {
        let unlocked = new Promise(cb);
        return unlocked.then(() => {
          this.queue.shift();
        });
      });

    this.queue.push(promise);
    if (this.queue.length === 1)
      resolve();
    else
      this.queue[this.queue.length - 2]
        .then(resolve)
        .catch(resolve);

    return promise;
  }

  wait(milsec: number) {
    return this.lock(unlock => wait(milsec).then(unlock));
  }

  private idTable: { [key: string]: { count: number; mutex: Mutex }} = {};
  waitWithId(id: string, milsec: number) {
    let obj = this.idTable[id];
    if (!obj) {
      obj = { count: 0, mutex: new Mutex() };
      this.idTable[id] = obj;
    }
    obj.count++;
    return obj.mutex.wait(milsec)
      .then(() => {
        obj.count--;
        if (obj.count === 0)
          delete this.idTable[id];
      });
  }
}
