import fs = require('fs');
import path = require('path');

interface Setting {
  app: {
    port: number;
  };
  db: {
    host: string;
    port: number;
    database: string;
    user: string;
    password: string;
  };
  dapi?: {
    host: string;
    port: number;
  }
}

export class Settings {
  static load(filename, dir = Settings.projectRoot, encoding = 'utf8') {
    let p = path.join(dir, filename);
    return <Setting>JSON.parse(fs.readFileSync(p, encoding));
  }

  static get projectRoot(): string {
    return process.env.PROJECT_ROOT;
  }
}
