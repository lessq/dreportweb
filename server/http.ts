import * as http from 'http';
import * as https from 'https';
import * as url from 'url';

import { log_error } from './utils';

export type HttpMethod = 'GET' | 'POST' | 'PUT' | 'DELETE';
export class HttpException extends Error {
  res: http.IncomingMessage;
  constructor(msg: string, res: http.IncomingMessage) {
    super(msg);
    this.res = res;
  }
}

type ContentType = 'json' | 'form';

export class Http {
  private http: typeof https;
  baseUrl: url.Url;
  baseRequestOption: https.RequestOptions;

  constructor(hostname: string | url.Url) {
    let urlObj = typeof hostname === 'string' ?
      url.parse(hostname):
      hostname;
    this.baseUrl = urlObj;
    this.baseRequestOption = {
      hostname: urlObj.hostname,
      port: parseInt(urlObj.port),
      path: urlObj.path,
      agent: false
    };
    if (urlObj.port)
      this.baseRequestOption.port = parseInt(urlObj.port);

    if (urlObj.protocol === 'http:')
      this.http = <any>http;
    else
      this.http = https;
  }

  private _request(method: HttpMethod, path: string, bodyObj?, isBuffer = false) {
    let body = JSON.stringify(bodyObj);
    return new Promise<any>((resolve, reject) => {
      let reqOpt: https.RequestOptions = method === 'GET' ?
        this.baseRequestOption:
        Object.assign({
          headers: {
            'Content-Type': 'application/json; charset=UTF-8',
            'Content-Length': Buffer.byteLength(body),
          }
        }, this.baseRequestOption);

      let req = this.http.request(Object.assign(reqOpt, {
        method: method,
        path: path
      }), res => {
        res.on('error', reject);
        if (isBuffer) {
          let buf: Buffer[] = [];
          res.on('data', buf.push.bind(buf))
            .on('end', () => {
            let ret = Buffer.concat(buf);
            if (res.statusCode >= 400)
              reject(new HttpException(ret.toString(), res));
            else
              resolve(ret);
          });
        } else {
          res.setEncoding('utf8');
          let str = '';
          res.on('data', chunk => str += chunk)
            .on('end', () => {
              if (res.statusCode >= 400)
                reject(new HttpException(str, res));
              else
                resolve(str);
            });
        }
      });
      req.on('error', reject);
      if (body)
        req.write(body);
      req.end();
    })
      .catch(e => {
        log_error(e);
        return Promise.reject(e);
      });
  }

  request(method: HttpMethod, path: string, bodyObj?): Promise<string> {
    return this._request(method, path, bodyObj, false);
  }

  requestBuffer(method: HttpMethod, path: string, bodyObj?): Promise<Buffer> {
    return this._request(method, path, bodyObj, true);
  }

  get(path: string, query?) {
    if (query)
      path += '?' +  Object.keys(query)
      .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(query[k]))
      .join('&');

    return this.request('GET', path);
  }

  post(path: string, body) {
    return this.request('POST', path, body);
  }

  put(path: string, body) {
    return this.request('PUT', path, body);
  }

  delete(path: string, body?) {
    return this.request('DELETE', path, body);
  }

  getBuffer(path: string, query?) {
    if (query)
      path += '?' +  Object.keys(query)
      .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(query[k]))
      .join('&');

    return this.requestBuffer('GET', path);
  }

  postBuffer(path: string, body) {
    return this.requestBuffer('POST', path, body);
  }

  putBuffer(path: string, body) {
    return this.requestBuffer('PUT', path, body);
  }

  deleteBuffer(path: string, body?) {
    return this.requestBuffer('DELETE', path, body);
  }
}
