export class Mutex {
  private queue: Promise<void>[] = [];

  lock(cb: (unlock: () => void) => void) {
    let resolve;
    let promise = (new Promise<Mutex>(r => {
      resolve = r;
    }))
      .then(() => {
        let unlocked = new Promise(cb);
        return unlocked.then(() => {
          this.queue.shift();
        });
      });

    this.queue.push(promise);
    if (this.queue.length === 1)
      resolve();
    else
      this.queue[this.queue.length - 2]
        .then(resolve)
        .catch(resolve);

    return promise;
  }
}
