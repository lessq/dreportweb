import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CenterService, Center } from '../admin/center.service';
import { UserService } from '../user.service';

@Component({
  selector: 'home',
  templateUrl: 'app/home/home.component.html'
})
export class HomeComponent implements OnInit {
  constructor(private router: Router,
              private route: ActivatedRoute,
              private centerService: CenterService,
              private userService: UserService) {}

  center: Center;
  isAdmin: boolean;
  private _rn: string;
  set receiptNumber(s: string) {
    this._rn = s;
    this.validateReceiptNumber();
  }
  get receiptNumber() {
    return this._rn;
  }
  valid = false;

  async ngOnInit() {
    this.route.params
      .switchMap(params => this.userService.getCurrentUser())
      .subscribe(user => this.center = Object.assign({ center_id: user.user_id}, user));

    this.isAdmin = await this.userService.isAdmin();
  }

  async validateReceiptNumber() {
    this.valid = false;
    if (await this.centerService.existsPDF(this._rn)) {
      this.valid = true;
      let cont = document.getElementById('pdf-container');
      cont.innerHTML = '';
      cont.appendChild(await this.centerService.getPDFImage(this._rn));
    }
  }

  onSubmit() {
    if (!this.valid)
      return;

    this.centerService.openPDF(this.receiptNumber);
  }
}
