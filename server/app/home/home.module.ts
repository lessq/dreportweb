import { NgModule }      from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { CenterService } from '../admin/center.service';
import { HomeComponent } from './home.component';
import { AdminModule } from '../admin/admin.module';

const homeRoutes: Routes = [
  { path: 'centers/:id', component: HomeComponent },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(homeRoutes)
  ],
  declarations: [
    HomeComponent,
  ],
  providers: [
    CenterService,
  ]
})
export class HomeModule {
}
