import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from './user.service';

class LoginUser {
  userId: string;
  password: string;
  valid = true;
}

@Component({
  selector: 'login',
  templateUrl: 'app/login.component.html'
})
export class LoginComponent {
  user = new LoginUser();
  failLogging = false;

  constructor(private userService: UserService,
              private router: Router) {}

  async onSubmit() {
    let loggedIn = await this.userService.login(this.user);

    if (loggedIn) {
      if (await this.userService.isAdmin())
        this.router.navigate(['/admin']);
      else {
        let u = await this.userService.getCurrentUser();
        this.router.navigate(['/centers/' + u.id]);
      }
    }
    else
      this.failLogging = true;
  }
}
