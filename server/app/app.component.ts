import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

import { UserService } from './user.service';

@Component({
  selector: 'my-app',
  templateUrl: 'app/app.component.html'
})
export class AppComponent implements OnInit {
  constructor(private userService: UserService,
              private router: Router) {}

  ngOnInit() {
    this.router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe(_ => {
        this.validateRoute();
      });
  }

  async validateRoute() {
    if (!await this.userService.isLoggingIn())
      this.router.navigate(['/login']);
  }
}

