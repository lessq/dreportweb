import { Component, OnInit } from '@angular/core';

import { CenterService, Center } from './center.service';

@Component({
  selector: 'admin',
  templateUrl: 'app/admin/admin.component.html'
})
export class AdminComponent implements OnInit {
  constructor(private centerService: CenterService) {}

  centers: Center[] = [];

  async ngOnInit() {
    this.centers = await this.centerService.getCenters();
  }
}
