import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Observable } from 'rxjs/Rx';

export interface Center {
  isNew?: boolean;
  id: number;
  center_id: string;
  name: string;
  address: string;
  password?: string;
}

const headers = new Headers({ 'Content-Type': 'application/json' });

@Injectable()
export class CenterService {
  constructor(private http: Http) {}

  getCenters(): Promise<Center[]> {
    // return this.http.get('/api/admin/centers')
    return this.http.get('/api/admin/users')
      .map(res => res.json())
      .do(json => json.forEach(u => u.center_id = u.user_name))
      .toPromise()
      .catch(() => null);
  }

  getCenter(id: number) {
    let nc: Center = <any>{ isNew: true };
    if (isNaN(id))
      return Observable.from([nc]);
    else
      return this.http.get(`/api/admin/centers/${id}`)
      .map(res => <Center>(res.json() || nc))
  }

  saveCenter(c: Center) {
    return this.http.post('/api/admin/centers',
                          JSON.stringify([{
                            id: c.id,
                            center_id: c.center_id,
                            name: c.name,
                            address: c.address,
                            password: c.password
                          }]),
                          {headers: headers}
                         )
      .toPromise()
      .then(() => true)
      .catch(() => false);
  }

  deleteCenter(c: Center) {
    return this.http.delete(`/api/admin/centers/${c.id}`)
      .map(res => res.json())
      .toPromise()
      .then(f => {
        if (f)
          return true;

        alert('Cannot dalete yourself.');
        return false;
      })
      .catch(e => {
        console.error(e);
        alert('Delete center was failed.');
        return false;
      });
  }

  getPDF(id: string) {
    return this.http.get('/api/pdf/' + encodeURIComponent(id));
  }

  getPDFImage(id: string) {
    let img = document.createElement('img');
    img.src = '/api/pdf/' + encodeURIComponent(id) + '/image';
    img.className = 'pdf-image';
    return img;
  }

  openPDF(id: string) {
    return window.open('/api/pdf/' + encodeURIComponent(id));
  }

  existsPDF(id: string) {
    return this.http.get('/api/pdf/' + encodeURIComponent(id) + '/exists')
      .map(res => res.json())
      .toPromise()
      .catch(() => false);
  }
}
