var path = require('path');
var fs = require('fs');
var gulp = require('gulp');
var ts = require('gulp-typescript');
var browsersync = require('browser-sync');
var sourcemaps = require('gulp-sourcemaps');
var childProcess = require('child_process');
var tsNameof = require('ts-nameof');
var server;

function shell(command, args) {
  return new Promise((resolve, reject) => {
    let env = Object.assign({}, process.env);
    var prjRoot = path.dirname(__filename);
    let ps = childProcess.spawn(command, args, {
      cwd: undefined,
      env: Object.assign(env, {
        PROJECT_ROOT: prjRoot
      }),
      stdio: 'inherit'
    });
    ps.on('exit', resolve);
    ps.on('error', reject);
  });
}

function angularBuild(isAot) {
  return new Promise((resolve, reject) => {
    fs.readFile('angular-cli.json', 'utf8', (err, data) => {
      if (err)
        return reject(err);

      let outDir;
      Promise.resolve(JSON.parse(data))
        .then(conf => new Promise((resolve, reject) => {
          conf.apps[0].root = 'tmp';
          outDir = conf.apps[0].outDir;
          conf.apps[0].outDir = 'desttmp';
          fs.writeFile('angular-cli.json', JSON.stringify(conf), (err) => {
            if (err)
              reject(err);
            else
              resolve();
          });
        }))
        .then(() => new Promise((resolve, reject) => {
          let args = ['build', '--watch', 'true'];
          if (isAot)
            args = args.concat(['--aot', 'true', '-prod']);
          let ps = childProcess.spawn('ng', args, {
            cwd: undefined,
            env: process.env,
            stdio: 'pipe'
          });
          cliProcess = ps;
          process.stdin.pipe(ps.stdin);
          ps.stdout.pipe(process.stdout);
          ps.stderr.pipe(process.stderr);
          ps.on('error', reject);
          ps.on('end', resolve);
          ps.stderr.once('data', () => {
            if (data)
              fs.writeFile('angular-cli.json', data, err => {
                data = null;
                if (err)
                  console.error(err);
              });
          });
          ps.stdout.once('data', line => {
            // angular-cli outputs to stdout when build finished.
            // It outputs progress to stderr.
            resolve(ps);
          });
        }))
        .then(ps => {
          return shell('rm', ['-rf', outDir])
            .then(() => shell('mv', ['desttmp', outDir]))
            .then(() => resolve(ps))
            .then(() => ps);
        })
        .catch(reject)
          .then(ps => {
            if (data)
              fs.writeFile('angular-cli.json', data, err => {
                if (err)
                  console.error(err);
              });
            resolve(ps);
          })
    });
  })
    .catch(console.error)
}


function spawn(args) {
  return new Promise(resolve => {
    var env = Object.assign({}, process.env);
    var prjRoot = path.dirname(__filename);
    var options = {
      cwd: prjRoot,
      env: Object.assign(env, {
        PROJECT_ROOT: prjRoot
      }),
      stdio: ['pipe', 'pipe', 'pipe'] // [stdin, stdout, stderr]
    };
    var server = childProcess.spawn('node', args, options);
    server.stdout.setEncoding('utf8');
    server.stdout.pipe(process.stdout);

    server.stderr.setEncoding('utf8');
    server.stderr.pipe(process.stderr);

    server.stdout.on('data', function listener() {
      resolve(server);
      server.stdout.removeListener('data', listener);
    });
    server.stderr.on('data', function listener() {
      resolve(server);
      server.stderr.removeListener('data', listener);
    });
  });
}

var serverProject = ts.createProject(path.resolve('./server/tsconfig.json'), { typescript: require('typescript') });
gulp.task('buildServer', function() {
  // return gulp.src(['server/**/*.ts'])
  return serverProject.src()
    .pipe(sourcemaps.init())
    .pipe(serverProject(ts.reporter.defaultReporter()))
    .js
    .pipe(sourcemaps.write('./sourcemap'))
    .pipe(gulp.dest(path.resolve('./server/dest/')));
});

gulp.task('resolve-name-of', function(cb) {
  if (cliProcess)
    cliProcess.kill('SIGSTOP');
  gulp.src(['src/**/*.ts'])
    .pipe(tsNameof())
    .pipe(gulp.dest(path.resolve('./tmp1')))
    .on('end', () => 
        gulp.src(['src/**/*', '!src/**/*.ts'])
        .pipe(gulp.dest(path.resolve('./tmp1')))
        .on('end', () => {
          shell('rm', ['-rf', './tmp'])
            .then(() => shell('mv', ['./tmp1', './tmp']))
            .then(() => {
              if (cliProcess)
                cliProcess.kill('SIGCONT');
            })
            .then(cb)
        }));
});
gulp.task('preBuildApp', ['resolve-name-of']);
gulp.task('buildApp', ['preBuildApp'], function(fin) {
  angularBuild()
    .then(ps => ps.kill())
    .catch(e => console.error(e))
      .then(() => fin());
});
gulp.task('buildAppAot', ['preBuildApp'], function(fin) {
  angularBuild(true)
    .then(ps => ps.kill())
    .catch(e => console.error(e))
      .then(() => fin());
});
var cliProcess;
gulp.task('buildAppWatch', ['preBuildApp'], function(fin) {
  // if (cliProcess)
  //   cliProcess.stdout.once('data', () => fin());
  if (cliProcess) {
    cliProcess.kill();
    cliProcess = null;
  }

  angularBuild()
  // .then(ps => cliProcess = ps) // cli takes too many memorry
    .then(ps => ps.kill())
    .catch(e => console.error(e))
      .then(() => fin());
});

gulp.task('build', ['buildApp', 'buildServer']);

gulp.task('browsersync', function() {
  browsersync({
    port: 4000,
    files: [
      './index.html',
      'dest/app/**/*.html',
      'dest/app/**/*.css',
    ],
    proxy: 'http://localhost:3000',
    open: false
  });
});

gulp.task('reloadBrowser', function() {
  browsersync.reload({stream:false});
});

var server;
gulp.task('runServer', function() {
  if (server) {
    server.kill('SIGKILL');
    server = null;
  }
  return spawn(['./server/dest/main.js'])
    .then(process => server = process);
});

gulp.task('build-and-run-server', ['buildServer'], function() {
  return gulp.run('runServer');
});

gulp.task('build-and-reload-app', ['buildAppWatch'], function() {
  return gulp.run('reloadBrowser');
});

gulp.task('preWatch', ['buildAppWatch', 'buildServer'], function() {
  return gulp.run('runServer');
});

gulp.task('watch', ['preWatch'], function() {
  gulp.watch(['./server/**/*.ts'], ['build-and-run-server']);
  gulp.watch(['./src/**/*'], ['build-and-reload-app']);
  gulp.run('browsersync');
});
