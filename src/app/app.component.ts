import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Location } from '@angular/common';

import { UserService } from './user.service';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  title: string;
  constructor(private userService: UserService,
              private router: Router,
              private location: Location) {}

  ngOnInit() {
    this.router.events
      .filter(event => event instanceof NavigationStart)
      .flatMap(_ => this.validateRoute())
      .subscribe(_ => this.setTitle());
  }

  async validateRoute() {
    if (!await this.userService.isLoggingIn())
      this.router.navigate(['/login']);
  }

  async setTitle() {
    if (!await this.userService.isLoggingIn() ||
        this.location.path().match(/^\/login/))
      this.title = "Center Manager";
    else {
      if (await this.userService.isAdmin())
        this.title = "Administrator";
      else {
        let user = await this.userService.getCurrentUser();
        this.title = `${user.name} (${user.id})`;
      }
    }
  }
}

