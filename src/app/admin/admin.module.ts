import { Component, NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AdminComponent } from './admin.component';
import { CenterDetailComponent } from './centerDetail.component';
import { CenterService } from './center.service';
import { UserService } from '../user.service';

@Component({
  selector: 'admin-base',
  template: `
<div *ngIf="!isAdmin">You are not an Administrator!!</div>
<div [hidden]="!isAdmin">
  <router-outlet></router-outlet>
</div>
`
})
export class AdminBase implements OnInit {
  constructor(private u: UserService) {}
  isAdmin;
  async ngOnInit() {
    this.isAdmin = await this.u.isAdmin();
  }
}

export const adminRoutes: Routes = [
  { path: 'admin',
    component: AdminBase,
    children: [
      { path: '', component: AdminComponent },
      { path: 'center/:id', component: CenterDetailComponent },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(adminRoutes)
  ],
  declarations: [
    AdminBase,
    AdminComponent,
    CenterDetailComponent
  ],
  providers: [
    CenterService
  ]
})
export class AdminModule {
}
