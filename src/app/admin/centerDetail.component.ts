import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { CenterService, Center } from './center.service';

class NewCenter implements Center {
  isNew: boolean;
  private _modifyPassword: boolean;
  set modifyPassword(f: boolean) {
    this._modifyPassword = f;
    this.validate();
  }
  get modifyPassword() {
    return this._modifyPassword;
  }
  private _id: number;
  set id(s: number) {
    this._id = typeof s === 'string' ? parseInt(s) : s;
    this.validate();
  }
  get id() {
    return this._id;
  }
  private _center_id: string;
  set center_id(s: string) {
    this._center_id = s;
    this.validate();
  }
  get center_id() {
    return this._center_id;
  }
  private _name: string;
  set name(s: string) {
    this._name = s;
    this.validate();
  }
  get name() {
    return this._name;
  }
  private _address: string;
  set address(s: string) {
    this._address = s;
    this.validate();
  }
  get address() {
    return this._address;
  }
  private _password: string;
  set password(p: string) {
    this._password = p;
    this.validate();
  }
  get password() {
    return this._password;
  }

  private _password1: string;
  set password1(p: string) {
    this._password1 = p;
    this.validate();
  }
  get password1() {
    return this._password1;
  }

  get showPassword() {
    return this.isNew || this.modifyPassword;
  }

  validPass = true;
  valid = false;
  validate() {
    if (this.showPassword) {
      this.validPass = this.password1 === this.password;
      return this.valid = !!(
        this.name &&
          this.center_id &&
          this.password &&
          this.password1 &&
          this.validPass
      );
    } else {
      this.validPass = true;
      return this.valid = !!(
        this.name &&
          this.center_id
      );
    }
  }
}

@Component({
  selector: 'detail',
  templateUrl: './centerDetail.component.html'
})
export class CenterDetailComponent implements OnInit {
  constructor(private centerService: CenterService,
              private location: Location,
              private route: ActivatedRoute,
              private router: Router) {}

  center: NewCenter;
  failed = false;

  @Input()
  set centerId(id: number) {
    this.centerService.getCenter(id)
      .map(center => {
        let ret = new NewCenter();
        Object.keys(center).forEach(k => ret[k] = center[k]);
        return ret;
      })
      .subscribe(c => {
        this.center = c;
      });
  }

  ngOnInit() {
    this.route.params
      .map(params => params['id'])
      .subscribe(id => {
        this.centerId = +id;
      });
  }

  async onSubmit() {
    let center = this.center;
    let stat = await this.centerService.saveCenter(
      center.showPassword ? {
        isNew: center.isNew,
        id: center.id,
        center_id: center.center_id,
        name: center.name,
        address: center.address,
        password: center.password
      } : {
        isNew: center.isNew,
        id: center.id,
        center_id: center.center_id,
        name: center.name,
        address: center.address
      }
    )
    if (stat)
      this.location.back();
    else
      this.failed = true;
  }

  onCancel() {
    this.location.back();
    return false;
  }

  onDelete() {
    this.centerService.deleteCenter(this.center)
      .then(f => {
        if (f)
          this.location.back();
      });
    return false;
  }
}
