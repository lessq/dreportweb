import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CenterService, Center } from './center.service';

@Component({
  selector: 'admin',
  templateUrl: './admin.component.html'
})
export class AdminComponent implements OnInit {
  constructor(private centerService: CenterService,
              private router: Router) {}

  centers: Center[] = [];

  async ngOnInit() {
    this.centers = await this.centerService.getCenters();
  }

  goCenter(center: Center) {
    this.router.navigateByUrl('/admin/center/' + center.id);
  }

  onDelete(center: Center, e: MouseEvent) {
    e.preventDefault();
    this.centerService.deleteCenter(center)
      .then(() => this.ngOnInit());
    return false;
  }
}
