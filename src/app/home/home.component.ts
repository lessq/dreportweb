import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CenterService, Center, Invoice } from '../admin/center.service';
import { UserService } from '../user.service';

interface HomeInvoice extends Invoice {
  encodedId: string;
}

@Component({
  selector: 'home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  constructor(private router: Router,
              private route: ActivatedRoute,
              private centerService: CenterService,
              private userService: UserService) {}

  center: Center;
  isAdmin: boolean;
  invoices: HomeInvoice[];

  private _rn: string;
  set receiptNumber(s: string) {
    this._rn = s;
    this.validateReceiptNumber();
  }
  get receiptNumber() {
    return this._rn;
  }
  valid = false;

  async ngOnInit() {
    this.route.params
      .switchMap(params => this.userService.getCurrentUser())
      .subscribe(user => this.center = Object.assign({ center_id: user.user_id}, user));

    this.isAdmin = await this.userService.isAdmin();
  }

  async validateReceiptNumber() {
    this.valid = false;
    if (this.receiptNumber === '')
      return;

    if (await this.centerService.existsInvoice(this.receiptNumber)) {
      this.valid = true;
      let invoices = (await this.centerService.getInvoices(this.receiptNumber));
      invoices.forEach((i: any) => i.encodedId = encodeURIComponent(i.id));
      this.invoices = <any>invoices;
    }
  }

  openPDF(invoiceId: string) {
    if (!this.valid)
      return;
    this.centerService.openPDF(invoiceId);
  }
}
