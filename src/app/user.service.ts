import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

export interface User {
  id: number;
  user_id: string;
  name: string;
  address: string;
}

export interface LoginUser {
  userId: string;
  password: string;
}

const headers = new Headers({ 'Content-Type': 'application/json' });

@Injectable()
export class UserService {
  constructor(private http: Http) {}

  getCurrentUser(): Promise<User> {
    return this.http.get('/api/login')
      .map(res => res.json())
      .toPromise()
      .catch(() => null);
  }

  isLoggingIn() {
    return this.getCurrentUser();
  }

  login(user: LoginUser) {
    return this.http.post('/api/login',
                          JSON.stringify({
                            userId: user.userId,
                            password: user.password
                          }),
                          {headers: headers})
      .toPromise()
      .then(() => true)
      .catch(() => false);
  }

  isAdmin(): Promise<boolean> {
    return this.http.get('/api/admin/isAdmin')
      .map(res => !!res.json())
      .toPromise()
      .catch(() => false);
  }
}
