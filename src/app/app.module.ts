import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AppComponent }   from './app.component';
import { LoginComponent } from './login.component';
import { HomeModule } from './home/home.module';
import { AdminModule } from './admin/admin.module';
import { UserService } from './user.service';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
];

@NgModule({
  imports:      [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpModule,
    FormsModule,
    HomeModule,
    AdminModule
  ],
  declarations: [
    AppComponent,
    LoginComponent,
  ],
  providers: [
    UserService
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule {
  
}

