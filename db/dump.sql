-- MySQL dump 10.13  Distrib 5.7.16, for Linux (x86_64)
--
-- Host: localhost    Database: dReport
-- ------------------------------------------------------
-- Server version	5.7.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Log`
--

DROP TABLE IF EXISTS `Log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Log` (
  `user_id` int(10) unsigned DEFAULT NULL,
  `http_method` char(8) DEFAULT NULL,
  `session_key` char(64) DEFAULT NULL,
  `ip` varchar(128) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Log`
--

LOCK TABLES `Log` WRITE;
/*!40000 ALTER TABLE `Log` DISABLE KEYS */;

/*!40000 ALTER TABLE `Log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Session`
--

DROP TABLE IF EXISTS `Session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Session` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `session_key` char(64) DEFAULT NULL,
  `expiry` datetime NOT NULL,
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_key` (`session_key`),
  KEY `user_id` (`user_id`),
  KEY `session_index` (`session_key`),
  CONSTRAINT `Session_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Session`
--

LOCK TABLES `Session` WRITE;
/*!40000 ALTER TABLE `Session` DISABLE KEYS */;

/*!40000 ALTER TABLE `Session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `salt` char(32) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  UNIQUE KEY `password` (`password`),
  KEY `user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'test','Test User','Test Address','test','37268335dd6931045bdcdf92623ff819a64244b53d0e746d438797349d4da578',NULL,'2016-11-18 11:26:08','2016-11-18 11:28:25'),(2,'ID A','Center A','Address A','CWEQNAHYQYKKSHKF','18269687ff669ecd7ca54536d4c51da704fb2b363752e158bc25a1b55ae6c73d',NULL,'2016-11-18 14:20:34','2016-11-20 09:26:53'),(3,'ID B','Center B','Address B','DTWMNSRCAFDRSFQY','cbd2ada4b6fcd062d10cb8fa41c294a0fe107867f22f864af15403a5b7cadd5d',NULL,'2016-11-18 14:21:33','2016-11-20 09:30:09'),(4,'ID C','CenterC','Address C','SCYFWTJKHYJMAMUK','233b01b7d75b1e75ae9ac5f7bde159a254291c7f278118bcd047fcc92dccc3b4',NULL,'2016-11-18 14:21:47','2016-11-20 09:28:02'),(5,'ID1','Center1','Address1','SJKRJYPECQLFVJHJ','10d0c601a9680180e39f7178c5d54a0e169b368cff89b34101547fa380319a43',NULL,'2016-11-18 14:22:26','2016-11-20 09:40:53'),(6,'ID2','Center2','Address2','TCEYEWAALHYSMDGR','de26f99e4534d89fd6e3618de67b113f633c9ca4ad4b171138dcb0c2d56d3935',NULL,'2016-11-18 14:25:08','2016-11-20 09:41:09'),(7,'ID3','Center3','Address3','CHYENCNVXQDXHNWG','3db0cdb0c61c504a3b2f6e0f1129ffa078495ae07f79409cab689d624aa6639d',NULL,'2016-11-18 14:25:46','2016-11-20 09:41:28'),(8,'ID4','Center4','Address4','LJMSVKQSFRRVSTRH','69eb0f19eb68d1afa26fb9d220a16719f5f5b3932d0daf96328ab409a9bea219',NULL,'2016-11-20 06:39:11','2016-11-20 09:41:55');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserType`
--

DROP TABLE IF EXISTS `UserType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserType` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `user_type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`user_type_id`),
  KEY `user_type_id` (`user_type_id`),
  CONSTRAINT `UserType_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`),
  CONSTRAINT `UserType_ibfk_2` FOREIGN KEY (`user_type_id`) REFERENCES `UserTypeMaster` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserType`
--

LOCK TABLES `UserType` WRITE;
/*!40000 ALTER TABLE `UserType` DISABLE KEYS */;
INSERT INTO `UserType` VALUES (1,1,1,'2016-11-18 11:37:02','2016-11-18 11:37:02'),(2,2,2,'2016-11-18 14:20:34','2016-11-18 14:20:34'),(3,3,2,'2016-11-18 14:21:33','2016-11-18 14:21:33'),(4,4,2,'2016-11-18 14:21:47','2016-11-18 14:21:47'),(5,5,2,'2016-11-18 14:22:26','2016-11-18 14:22:26'),(6,6,2,'2016-11-18 14:25:08','2016-11-18 14:25:08'),(7,7,2,'2016-11-18 14:25:46','2016-11-18 14:25:46'),(8,8,2,'2016-11-20 06:39:11','2016-11-20 06:39:11');
/*!40000 ALTER TABLE `UserType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserTypeMaster`
--

DROP TABLE IF EXISTS `UserTypeMaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserTypeMaster` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(63) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserTypeMaster`
--

LOCK TABLES `UserTypeMaster` WRITE;
/*!40000 ALTER TABLE `UserTypeMaster` DISABLE KEYS */;
INSERT INTO `UserTypeMaster` VALUES (1,'Administrator','Administrator','2016-11-18 11:36:38','2016-11-18 11:36:38'),(2,'Center','Center','2016-11-18 14:20:27','2016-11-18 14:20:27');
/*!40000 ALTER TABLE `UserTypeMaster` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-20 18:49:59
