DROP TABLE IF EXISTS Log;
DROP TABLE IF EXISTS Session;
DROP TABLE IF EXISTS UserType;
DROP TABLE IF EXISTS User;
DROP TABLE IF EXISTS UserTypeMaster;

CREATE TABLE UserTypeMaster (
  id int unsigned NOT NULL AUTO_INCREMENT,
  type varchar(63) NOT NULL UNIQUE,
  description varchar(255),
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);

CREATE TABLE User (
  id int unsigned NOT NULL AUTO_INCREMENT,
  user_id varchar(255) NOT NULL UNIQUE,
  name varchar(255) NOT NULL,
  address varchar(255),
  salt char(32),
  password char(64) UNIQUE,
  enabled boolean,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  INDEX user_id_index (user_id)
);

CREATE TABLE UserType (
  id int unsigned NOT NULL AUTO_INCREMENT,
  user_id int unsigned NOT NULL,
  user_type_id int unsigned NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE (user_id, user_type_id),
  FOREIGN KEY (user_id) REFERENCES User(id),
  FOREIGN KEY (user_type_id) REFERENCES UserTypeMaster(id),
);

CREATE TABLE Session (
  id int unsigned NOT NULL AUTO_INCREMENT,
  user_id int unsigned NOT NULL,
  session_key char(64) UNIQUE,
  expiry DATETIME NOT NULL,
  closed boolean NOT NULL DEFAULT false,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES User(id),
  INDEX session_index (session_key)
);

CREATE TABLE Log (
  user_id int unsigned,
  http_method char(8),
  session_key char(64),
  ip varchar(128),
  user_name varchar(255),
  path varchar(255),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
